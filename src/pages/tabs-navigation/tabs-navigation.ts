import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

/**
 * Generated class for the TabsNavigationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs-navigation',
  templateUrl: 'tabs-navigation.html',
})
export class TabsNavigationPage {

  tab1Root: any;
  tab2Root: any;
  tab3Root: any;

  constructor() {
  	this.tab1Root = "ListingPage";
  	this.tab2Root = "ProfilePage";
  	this.tab3Root = "NotificationsPage";
  }

}
