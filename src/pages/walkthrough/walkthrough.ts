import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';

/**
 * Generated class for the WalkthroughPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {

  lastSlide = false;

  @ViewChild('slider') slider: Slides;

  constructor(
  	public navCtrl: NavController,
  	public navParams: NavParams
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalkthroughPage');
  }

  skipIntro() {
  	// You can skip to main app
  	// this.nav.setRoot("TabsNavigationPage");

  	// Or you can skip to last slide (login/signup slide)
  	this.lastSlide = true;
  	this.slider.slideTo(this.slider.length());
  }

  onSlideChanged() {
  	// If it's the last slide, then hide the 'Skip' button on the header
  	this.lastSlide = this.slider.isEnd();
  }

  goToLogin() {
  	this.navCtrl.push("LoginPage");
  }

  goToSignup() {
  	this.navCtrl.push("SignupPage");
  }

}
