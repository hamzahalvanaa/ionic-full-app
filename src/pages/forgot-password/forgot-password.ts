import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  forgot_password: FormGroup;
  main_page: { component: any };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.main_page = { component: "TabsNavigationPage" };

    this.forgot_password = new FormGroup({
      email: new FormControl('', Validators.required)
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPasswordPage');
  }

  recoverPassword() {
    console.log(this.forgot_password.value);
    this.navCtrl.setRoot(this.main_page.component);
  }

}
