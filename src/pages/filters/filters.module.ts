import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FiltersPage } from './filters';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FiltersPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(FiltersPage),
  ],
})
export class FiltersPageModule {}
