import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import 'rxjs/Rx';
import { NotificationsModel } from './notifications.model';
import { NotificationsService } from './notifications.service';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {
  notifications: NotificationsModel = new NotificationsModel();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public notificationsService: NotificationsService
  ) {
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.notificationsService
      .getData()
      .then(data => {
        this.notifications.today = data.today;
        this.notifications.yesterday = data.yesterday;
          this.loading.dismiss();
    });
    console.log('ionViewDidLoad NotificationsPage');
  }

}
