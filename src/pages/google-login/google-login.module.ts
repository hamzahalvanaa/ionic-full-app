import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoogleLoginPage } from './google-login';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GoogleLoginPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(GoogleLoginPage),
  ],
})
export class GoogleLoginPageModule {}
