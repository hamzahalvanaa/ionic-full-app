import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { GoogleUserModel } from './google-user.model';
import { GoogleLoginService } from './google-login.service';

/**
 * Generated class for the GoogleLoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-google-login',
  templateUrl: 'google-login.html',
})
export class GoogleLoginPage {
  user: GoogleUserModel = new GoogleUserModel();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public googleLoginService: GoogleLoginService,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    let env = this;

    this.googleLoginService.getGoogleUser().then(function(user){
      env.user = user;
      env.loading.dismiss();
    }, function(error){
      console.log(error);
      env.loading.dismiss();
    });
    console.log('ionViewDidLoad GoogleLoginPage');
  }

  doGoogleLogout() {
    let env = this;

    this.googleLoginService.doGoogleLogout()
    .then(function(res) {
      env.user = new GoogleUserModel();
    }, function(error){
      console.log("Google logout error", error);
    });
  }

  doGoogleLogin() {
    let env = this;

    this.googleLoginService.doGoogleLogin()
    .then(function(user){
      env.user = user;
    }, function(err){
      console.log("Google Login error", err);
    });
  }

}
