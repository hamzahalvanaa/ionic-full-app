import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, App, LoadingController, SegmentButton } from 'ionic-angular';
import 'rxjs/Rx';
import { ProfileModel } from './profile.model';
import { ProfileService } from './profile.service';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  display: string;
  profile: ProfileModel = new ProfileModel();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public app: App,
    public profileService: ProfileService,
    public loadingCtrl: LoadingController,
    private socialSharing: SocialSharing
  ) {
    this.display = 'list';
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.profileService.getData().then(data => {
      this.profile.user       = data.user;
      this.profile.following  = data.following;
      this.profile.followers  = data.followers;
      this.profile.posts      = data.posts;
      this.loading.dismiss();
    });
    console.log('ionViewDidLoad ProfilePage');
  }

  goToFollowersList() {
    // close the menu when clicking a link from the menu
    this.menuCtrl.close();
    this.app.getRootNav().push("FollowersPage", { list: this.profile.followers });
  }

  goToFollowingList() {
    // close the menu when clicking a link from the menu
    this.menuCtrl.close();
    this.app.getRootNav().push("FollowersPage", { list: this.profile.following });
  }

  goToSettings() {
    // close the menu when clicking a link from the menu
    this.menuCtrl.close();
    this.app.getRootNav().push("SettingsPage");
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }

  sharePost(post) {
   //this code is to use the social sharing plugin
   // message, subject, file, url
   this.socialSharing.share(post.description, post.title, post.image)
   .then(() => {
     console.log('Success!');
   })
   .catch(() => {
      console.log('Error');
   });
  }

}
