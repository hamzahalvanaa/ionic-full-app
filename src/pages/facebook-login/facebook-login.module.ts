import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FacebookLoginPage } from './facebook-login';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FacebookLoginPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(FacebookLoginPage),
  ],
})
export class FacebookLoginPageModule {}
