import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { List1Page } from './list1';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    List1Page,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(List1Page),
  ],
})
export class List1PageModule {}
