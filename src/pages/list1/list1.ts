import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import 'rxjs/Rx';
import { List1Model } from './list-1.model';
import { List1Service } from './list-1.service';

/**
 * Generated class for the List1Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list1',
  templateUrl: 'list1.html',
})
export class List1Page {
  list1: List1Model = new List1Model();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public list1Service: List1Service,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.list1Service
      .getData()
      .then(data => {
        this.list1.items = data.items;
        this.loading.dismiss();
    });
    console.log('ionViewDidLoad List1Page');
  }

}
