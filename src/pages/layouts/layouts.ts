import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LayoutsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-layouts',
  templateUrl: 'layouts.html',
})
export class LayoutsPage {
  items: Array<{title: string, note?: string, component: any}>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.items = [
      { title: 'Schedule', component: "SchedulePage" },
      { title: 'Lists', note: '(Big)', component: "List1Page" },
      { title: 'Lists', note: '(Mini)', component: "List2Page" },
      { title: 'Grid', component: "GridPage" },
      { title: 'Notifications', component: "NotificationsPage" },
      { title: 'Profile', component: "ProfilePage" }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LayoutsPage');
  }

  itemTapped(event, item) {
    this.navCtrl.push(item.component);
  }

}
