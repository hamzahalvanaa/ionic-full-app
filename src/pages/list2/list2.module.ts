import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { List2Page } from './list2';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    List2Page,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(List2Page),
  ],
})
export class List2PageModule {}
