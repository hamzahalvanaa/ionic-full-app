import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import 'rxjs/Rx';
import { List2Model } from './list-2.model';
import { List2Service } from './list-2.service';

/**
 * Generated class for the List2Page page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-list2',
  templateUrl: 'list2.html',
})
export class List2Page {
  list2: List2Model = new List2Model();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public list2Service: List2Service,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.list2Service
      .getData()
      .then(data => {
        this.list2.items = data.items;
        this.loading.dismiss();
    });
    console.log('ionViewDidLoad List2Page');
  }

}
