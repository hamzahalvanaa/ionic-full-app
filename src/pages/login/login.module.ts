import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(LoginPage),
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class LoginPageModule {}
