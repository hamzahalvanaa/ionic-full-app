import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';

import { FacebookLoginService } from '../facebook-login/facebook-login.service';
import { GoogleLoginService } from '../google-login/google-login.service';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  login: FormGroup;
  main_page: { component: any };
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public facebookLoginService: FacebookLoginService,
    public googleLoginService: GoogleLoginService
  ) {
    this.main_page = { component: "TabsNavigationPage" };

    this.login = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  doLogin() {
    this.navCtrl.setRoot(this.main_page.component);
  }

  doFacebookLogin() {
    this.loading = this.loadingCtrl.create();

    // Here we will check if the user is already logged in because we don't want to ask users
    // to log in each time they open the app
    let env = this;
    // env.navCtrl.setRoot(env.main_page.component);

    this.facebookLoginService.getFacebookUser().then(function(data) {
      // user is previously logged with Facebook and we have his data we will let him access the app
      env.navCtrl.setRoot(env.main_page.component);
    }, function(error) {
      // we don't have the user data so we will ask him to log in
      env.facebookLoginService.doFacebookLogin().then(function(res) {
        env.loading.dismiss();
        env.navCtrl.setRoot(env.main_page.component);
      }, function(err) {
        console.log("Facebook login error", err);
      });
    });
  }

  doGoogleLogin() {
    this.loading = this.loadingCtrl.create();

    // Here we will check if the user is already logged in because we don't want to ask users
    // to log in each time they open the app
    let env = this;

    this.googleLoginService.trySilentLogin().then(function(data) {
      // user is previously logged with Google and we have his data we will let him access the app
      env.navCtrl.setRoot(env.main_page.component);
    }, function(error) {
      // we don't have the user data so we will ask him to log in
      env.googleLoginService.doGoogleLogin().then(function(res) {
        env.loading.dismiss();
        env.navCtrl.setRoot(env.main_page.component);
      }, function(err) {
        console.log("Google login error", err);
      });
    });
  }

  goToSignup() {
    this.navCtrl.push("SignupPage");
  }

  goToForgotPassword() {
    this.navCtrl.push("ForgotPasswordPage");
  }

}
