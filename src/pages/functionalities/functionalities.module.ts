import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FunctionalitiesPage } from './functionalities';

@NgModule({
  declarations: [
    FunctionalitiesPage,
  ],
  imports: [
    IonicPageModule.forChild(FunctionalitiesPage),
  ],
})
export class FunctionalitiesPageModule {}
