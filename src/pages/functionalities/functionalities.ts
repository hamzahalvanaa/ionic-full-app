import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FunctionalitiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-functionalities',
  templateUrl: 'functionalities.html',
})
export class FunctionalitiesPage {
  items: Array<{title: string, note?: string, component: any}>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.items = [
      { title: 'Facebook Integration', component: "FacebookLoginPage" },
      { title: 'Google Integration', component: "GoogleLoginPage" },
      { title: 'Contact Card', component: "ContactCardPage" },
      { title: 'Maps', component: "MapsPage" }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FunctionalitiesPage');
  }

  itemTapped(event, item) {
    this.navCtrl.push(item.component);
  }

}
