import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ContactModel } from './contact.model';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SocialSharing } from '@ionic-native/social-sharing';

/**
 * Generated class for the ContactCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-card',
  templateUrl: 'contact-card.html',
})
export class ContactCardPage {
  contact: ContactModel = new ContactModel();

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private callNumber: CallNumber,
    private iab: InAppBrowser,
    private socialSharing: SocialSharing
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactCardPage');
  }

  call(number: string) {
    this.callNumber.callNumber(number, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
  }

  sendMail(email: string) {
    this.socialSharing.canShareViaEmail().then(() => {
      this.socialSharing.shareViaEmail("Hello, I'm trying this fantastic app that will save me hours of development.", "This app is the best!", [email]).then(() => {
        console.log('Success!');
      }).catch(() => {
        console.log('Error');
      });
    }).catch(() => {
       console.log('Sharing via email is not possible');
    });
  }

  openInAppBrowser() {
    const browser = this.iab.create('http://google.com');
    browser.on('loadstop').subscribe(event => {
      browser.insertCSS({ code: "body{color: red;}"})
    });
    browser.close();
  }

}
