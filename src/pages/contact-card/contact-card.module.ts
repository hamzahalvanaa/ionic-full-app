import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactCardPage } from './contact-card';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ContactCardPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ContactCardPage),
  ],
})
export class ContactCardPageModule {}
