import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import 'rxjs/Rx';
import { ListingModel } from './listing.model';
import { ListingService } from './listing.service';

/**
 * Generated class for the ListingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-listing',
  templateUrl: 'listing.html',
})
export class ListingPage {

  listing: ListingModel = new ListingModel();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public listingService: ListingService,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.listingService.getData().then(data => {
      this.listing.banner_image = data.banner_image;
      this.listing.banner_title = data.banner_title;
      this.listing.populars     = data.populars;
      this.listing.categories   = data.categories;
      this.loading.dismiss();
    });
    console.log('ionViewDidLoad ListingPage');
  }

  goToFeed(category: any) {
    this.navCtrl.push("FeedPage", { category: category });
    console.log("Clicked goToFeed", category);
  }
}
