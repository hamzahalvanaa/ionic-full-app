import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListingPage } from './listing';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    ListingPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(ListingPage),
  ],
})
export class ListingPageModule {}
