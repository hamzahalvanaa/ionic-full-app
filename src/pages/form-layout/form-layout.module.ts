import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormLayoutPage } from './form-layout';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    FormLayoutPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(FormLayoutPage),
  ],
})
export class FormLayoutPageModule {}
