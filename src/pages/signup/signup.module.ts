import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignupPage } from './signup';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SignupPage
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SignupPage)
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SignupPageModule {}
