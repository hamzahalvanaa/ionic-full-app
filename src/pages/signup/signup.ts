import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { FacebookLoginService } from '../facebook-login/facebook-login.service';
import { GoogleLoginService } from '../google-login/google-login.service';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  signup: FormGroup;
  main_page: { component: any };
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public facebookLoginService: FacebookLoginService,
    public googleLoginService: GoogleLoginService,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController
  ) {
    this.main_page = { component: "TabsNavigationPage" };

    this.signup = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirm_password: new FormControl('', Validators.required)
    });
  }

  doSignup() {
    this.navCtrl.setRoot(this.main_page.component);
  }

  doFacebookSignup() {
    this.loading = this.loadingCtrl.create();
    // Here we will check if the user is already logged in
    // because we don't want to ask users to log in each time they open the app
    let env = this;

    this.facebookLoginService.getFacebookUser().then(function(data) {
      // user is previously logged with Facebook and we have his data we will let him access the app
      env.navCtrl.setRoot(env.main_page.component);
    }, function(error) {
      // we don't have the user data so we will ask him to log in
      env.facebookLoginService.doFacebookLogin().then(function(res) {
        env.loading.dismiss();
        env.navCtrl.setRoot(env.main_page.component);
      }, function(err) {
        console.log("Facebook login error", err);
        env.loading.dismiss();
      });
    });
  }

  doGoogleSignup() {
    this.loading = this.loadingCtrl.create();

    // Here we will check if the user is already logged in
    // because we don't want to ask users to log in each time they open the app
    let env = this;

    this.googleLoginService.trySilentLogin()
    .then(function(data) {
       // user is previously logged with Google and we have his data we will let him access the app
      env.navCtrl.setRoot(env.main_page.component);
    }, function(error){
      //we don't have the user data so we will ask him to log in
      env.googleLoginService.doGoogleLogin()
      .then(function(res){
        env.loading.dismiss();
        env.navCtrl.setRoot(env.main_page.component);
      }, function(err){
        console.log("Google login error", err);
        env.loading.dismiss();
      });
    });
  }

  showTermsModal() {
    let modal = this.modalCtrl.create("TermsOfServicePage");
    modal.present();
  }

  showPrivacyModal() {
    let modal = this.modalCtrl.create("PrivacyPolicyPage");
    modal.present();    
  }

}
