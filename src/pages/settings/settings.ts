import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import 'rxjs/Rx';
import { FormGroup, FormControl } from '@angular/forms';
import { ProfileModel } from '../profile/profile.model';
import { ProfileService } from '../profile/profile.service';
import { Facebook } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  settingsForm: FormGroup;
  // make WalkthroughPage the root (or first) page
  rootPage: any = "WalkthroughPage";
  loading: any;
  profile: ProfileModel = new ProfileModel();

  user: any;
  userReady: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    public profileService: ProfileService,
    public fb: Facebook,
    public nativeStorage: NativeStorage
  ) {
    this.loading = this.loadingCtrl.create();
    
    this.settingsForm = new FormGroup({
      name: new FormControl(),
      location: new FormControl(),
      description: new FormControl(),
      currency: new FormControl(),
      weather: new FormControl(),
      notifications: new FormControl()
    });
  }

  ionViewCanEnter() {
    let env = this;
    this.nativeStorage.getItem('facebook_user')
    .then(function(data) {
      env.user = {
        name: data.name,
        gender: data.gender,
        picture: data.picture
      };
        env.userReady = true;
    }, function(error) {
      console.log(error);
    });
  }

  ionViewDidLoad() {
    this.loading.present();
    this.profileService.getData().then(data => {
      this.profile.user = data.user;

      this.settingsForm.setValue({
        name: data.user.name,
        location: data.user.location,
        description: data.user.about,
        currency: 'dollar',
        weather: 'fahrenheit',
        notifications: true
      });

      this.loading.dismiss();
    });
    console.log('ionViewDidLoad SettingsPage');
  }

  logout() {
    // navigate to the new page if it is not the current page
    this.navCtrl.setRoot(this.rootPage);
  }

  doFbLogout() {
    var nav = this.navCtrl;
    let env = this;
    this.fb.logout().then(function(response) {
      // user logged out so we will remove him from the NativeStorage
      env.nativeStorage.remove('facebook_user');
      nav.setRoot("LoginPage");
    }, function(error) {
      console.log(error);
    });
  }

  showTermsModal() {
    let modal = this.modalCtrl.create("TermsOfServicePage");
    modal.present();
  }

  showPrivacyModal() {
    let modal = this.modalCtrl.create("PrivacyPolicyPage");
    modal.present();
  }

}
