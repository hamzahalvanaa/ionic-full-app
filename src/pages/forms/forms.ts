import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the FormsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forms',
  templateUrl: 'forms.html',
})
export class FormsPage {
  items: Array<{title: string, note?: string, component: any}>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.items = [
      { title: 'Forms Examples', component: "FormLayoutPage" },
      { title: 'Filters', component: "FiltersPage" }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormsPage');
  }

  itemTapped(event, item) {
    this.navCtrl.push(item.component);
  }
}
