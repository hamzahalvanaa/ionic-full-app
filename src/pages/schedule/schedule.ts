import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, SegmentButton } from 'ionic-angular';
import 'rxjs/Rx';
import { ScheduleModel } from './schedule.model';
import { ScheduleService } from './schedule.service';

/**
 * Generated class for the SchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedule',
  templateUrl: 'schedule.html',
})
export class SchedulePage {
  segment: string;
  schedule: ScheduleModel = new ScheduleModel();
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public scheduleService: ScheduleService,
    public loadingCtrl: LoadingController
  ) {
    this.segment = "today";
    this.loading = this.loadingCtrl.create();
  }

  ionViewDidLoad() {
    this.loading.present();
    this.scheduleService
      .getData()
      .then(data => {
        this.schedule.today = data.today;
        this.schedule.upcoming = data.upcoming;
        this.loading.dismiss();
    });
    console.log('ionViewDidLoad SchedulePage');
  }

  onSegmentChanged(segmentButton: SegmentButton) {
    // console.log('Segment changed to', segmentButton.value);
  }

  onSegmentSelected(segmentButton: SegmentButton) {
    // console.log('Segment selected', segmentButton.value);
  }

}
