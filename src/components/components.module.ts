import { NgModule } from '@angular/core';
import { BackgroundImageComponent } from './background-image/background-image';
import { ColorRadioComponent } from './color-radio/color-radio';
import { CounterInputComponent } from './counter-input/counter-input';
import { GoogleMapComponent } from './google-map/google-map';
import { PreloadImageComponent } from './preload-image/preload-image';
import { RatingComponent } from './rating/rating';
import { ShowHideContainer } from './show-hide-password/show-hide-container';
import { ShowHideInput } from './show-hide-password/show-hide-input';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [
        BackgroundImageComponent,
        ColorRadioComponent,
        CounterInputComponent,
        GoogleMapComponent,
        PreloadImageComponent,
        RatingComponent,
        ShowHideContainer,
        ShowHideInput
    ],
	imports: [
        IonicModule
    ],
	exports: [
        BackgroundImageComponent,
        ColorRadioComponent,
        CounterInputComponent,
        GoogleMapComponent,
        PreloadImageComponent,
        RatingComponent,
        ShowHideContainer,
        ShowHideInput
    ]
})
export class ComponentsModule {}
