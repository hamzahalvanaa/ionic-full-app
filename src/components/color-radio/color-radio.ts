import { Directive, ElementRef, Input, Renderer } from '@angular/core';

@Directive({
	selector: '[color-radio]'
})

/**
 * Generated class for the ColorRadioComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */

export class ColorRadioComponent {

  @Input('color-radio') color: string;

  constructor(
  	public elementRef: ElementRef,
  	public renderer: Renderer
  ) {
    console.log('Hello ColorRadioComponent Component');
  }

  setColor(color: string) {
  	this.renderer.setElementStyle(this.elementRef.nativeElement, 'backgroundColor', color);
  	this.renderer.setElementStyle(this.elementRef.nativeElement, 'borderColor', color);
  }

  ngOnInit() {
  	this.setColor(this.color);
  	console.log(this.color);
  }
}
