import { Component, Input, ElementRef, Renderer, OnChanges, SimpleChange } from '@angular/core';

import { isPresent } from 'ionic-angular/util/util';

/**
 * Generated class for the PreloadImageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'preload-image',
  templateUrl: 'preload-image.html'
})
export class PreloadImageComponent implements OnChanges {

  _src: string = '';
  _ratio: { w: number, h: number };
  _img: any;

  constructor(
  	public elementRef: ElementRef,
  	public renderer: Renderer
  ) {
  	this._img = new Image();
    console.log('Hello PreloadImageComponent Component');
  }

	@Input() alt: string;

  @Input() title: string;

	@Input() set src(val: string) {
    this._src = isPresent(val) ? val : '';
  }

	@Input() set ratio(ratio: { w: number, h: number }){
		this._ratio = ratio || null;
  }

  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
    let ratio_height = (this._ratio.h / this._ratio.w * 100)+"%";
		// Conserve aspect ratio (see: http://stackoverflow.com/a/10441480/1116959)
		this.renderer.setElementStyle(this.elementRef.nativeElement, 'padding-bottom', ratio_height);

    this._update();
    // console.log("CHANGES preload-image", this._src);
    // console.log(changes['src'].isFirstChange());
  }

	_update() {
	  if (isPresent(this.alt)) {
	    this._img.alt = this.alt;
	  }
	  if (isPresent(this.title)) {
	    this._img.title = this.title;
	  }

	  this._img.addEventListener('load', () => {
      this.elementRef.nativeElement.appendChild(this._img);
			this._loaded(true);
	  });

	  this._img.src = this._src;

	  this._loaded(false);
	}

	_loaded(isLoaded: boolean) {
    this.elementRef.nativeElement.classList[isLoaded ? 'add' : 'remove']('img-loaded');
  }

}
