import { Component, Input, ElementRef, Renderer, OnChanges, SimpleChange } from '@angular/core';
import { isPresent } from 'ionic-angular/util/util';

/**
 * Generated class for the BackgroundImageComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'background-image',
  templateUrl: 'background-image.html'
})
export class BackgroundImageComponent implements OnChanges {
  
  _src: string = '';

  constructor(
  	public elementRef: ElementRef,
  	public renderer: Renderer
  ) {
    console.log('Hello BackgroundImageComponent Component');
  }

  @Input() class: string;

  @Input() set src(val: string) {
  	this._src = isPresent(val) ? val : '';
  }

  ngOnChanges(changes: { [propName: string]: SimpleChange }) {
  	this.update();
  	// console.log("CHANGES background-image", this._src);
    // console.log(changes['src'].isFirstChange());
  }

  update() {
  	let img = new Image();

  	img.addEventListener('load', () => {
  		this.elementRef.nativeElement.style.backgroundImage = 'url(' + this._src + ')';
  		this.loaded(true);
  	});

  	img.src = this._src;

  	this.loaded(false);
  }

  loaded(isLoaded: boolean) {
  	this.elementRef.nativeElement.classList[isLoaded ? 'add' : 'remove']('img-loaded');
  }

}
