import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { FacebookLoginService } from '../pages/facebook-login/facebook-login.service';
import { GoogleLoginService } from '../pages/google-login/google-login.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { NativeStorage } from '@ionic-native/native-storage';
import { ListingService } from '../pages/listing/listing.service';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ProfileService } from '../pages/profile/profile.service';
import { NotificationsService } from '../pages/notifications/notifications.service';
import { CallNumber } from '@ionic-native/call-number';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Keyboard } from '@ionic-native/keyboard';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMapsService } from '../pages/maps/maps.service';
import { ScheduleService } from '../pages/schedule/schedule.service';
import { List1Service } from '../pages/list1/list-1.service';
import { List2Service } from '../pages/list2/list-2.service';
import { FeedService } from '../pages/feed/feed.service';
import { DatePicker } from '@ionic-native/date-picker';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    FacebookLoginService,
    GoogleLoginService,
    ListingService,
    ProfileService,
    NotificationsService,
    GoogleMapsService,
    ScheduleService,
    List1Service,
    List2Service,
    FeedService,
    Facebook,
    GooglePlus,
    NativeStorage,
    SocialSharing,
    CallNumber,
    InAppBrowser,
    Keyboard,
    Geolocation,
    DatePicker,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule {}
