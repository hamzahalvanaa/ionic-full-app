import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NativeStorage } from '@ionic-native/native-storage';

// import { HomePage } from '../pages/home/home';
@Component({
  selector: 'app-root',
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  // make WalkthroughPage thr root (or first) page
  rootPage:any = "WalkthroughPage";

  pages: Array<{title: string, icon: string, component: any}>;
  pushPages: Array<{title: string, icon: string, component: any}>;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private menu: MenuController,
    private app: App,
    private nativeStorage: NativeStorage
  ) {
    this.initializeApp();

    this.pages = [
      { title: 'Home', icon: 'home', component: "TabsNavigationPage" },
      { title: 'Forms', icon: 'create', component: "FormsPage" },
      { title: 'Functionalities', icon: 'code', component: "FunctionalitiesPage" },
    ];

    this.pushPages = [
      { title: 'Layouts', icon: 'grid', component: "LayoutsPage" },
      { title: 'Settings', icon: 'settings', component: "SettingsPage" }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      let env = this;
      this.nativeStorage.getItem('facebook_user')
      .then(function(data) {
        // user is previously logged and we have his data
        // we will let him access the app
        env.nav.push("TabsNavigationPage");
        env.splashScreen.hide();
      }, function(error) {
        // we don't have the user data so we will ask him to log in
        env.nav.push("LoginPage");
        env.splashScreen.hide();
      });
      this.statusBar.styleLightContent();
    });
  }

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(page.component);
  }

  pushPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
    this.app.getRootNav().push(page.component);
  }

}
